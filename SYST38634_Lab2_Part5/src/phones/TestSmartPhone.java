package phones;

import static org.junit.Assert.*;

import org.junit.Test;

/*
 * 
 * @author C.Jack Jozwik
 * 
 * */

public class TestSmartPhone {

	//FormattedPrice testing
	
	@Test
	public void testGetFormattedPriceRegular() {
		SmartPhone phone = new SmartPhone();
		
		phone.setPrice(100);
		String fPrice = phone.getFormattedPrice();
		
		assertTrue("Formated price does not match.", fPrice.equals("$100"));
	}
	
	
	@Test
	public void testGetFormattedPriceBoundaryIn() {		
		SmartPhone phone = new SmartPhone();
		String fPrice = phone.getFormattedPrice();
		
		assertTrue("Formated price does not match.", fPrice.equals("$0"));
	}
	
	@Test
	public void testGetFormattedPriceBoundaryOut() {
		SmartPhone phone = new SmartPhone();
		
		phone.setPrice(-100);
		String fPrice = phone.getFormattedPrice();
		
		assertTrue("Formated price does not match.", fPrice.equals("$-100"));
	}
	
	@Test
	public void testGetFormattedPriceException() throws Exception {
		// Not sure how to cause exception with this get method?
		/*
		 * SmartPhone phone = new SmartPhone();
		 * 
		 * phone.getFormattedPrice();
		 * 
		 * if (!phone.getFormattedPrice().startsWith("$")) { throw new Exception(); }
		 * 
		 * fail("Formatted price is not valid");
		 */
	}
	
	//Version testing
	
	@Test
	public void testSetVersionRegular() throws VersionNumberException {
		SmartPhone phone = new SmartPhone();
		
		phone.setVersion(1.0);
		double version = phone.getVersion();
		
		assertTrue("Version number does not match.", version == 1.0);
	}
	
	@Test (expected = VersionNumberException.class)
	public void testSetVersionException() throws VersionNumberException {
		SmartPhone phone = new SmartPhone();
		
		phone.setVersion(4.1);
		
		fail("Version number is not valid");
	}
	
	@Test
	public void testSetVersionBoundaryIn() throws VersionNumberException {
		SmartPhone phone = new SmartPhone();
		
		phone.setVersion(4.0);
		double version = phone.getVersion();
		
		assertTrue("Version number does not match.", version == 4.0);
	}
	
	@Test
	public void testSetVersionBoundaryOut() throws VersionNumberException {
		SmartPhone phone = new SmartPhone();
		
		phone.setVersion(-1);
		double version = phone.getVersion();
		
		assertTrue("Version number does not match.", version == -1);
	}

}
